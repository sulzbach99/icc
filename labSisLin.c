#include <stdio.h>
#include <math.h>
#include <string.h>

#include "utils.h"
#include "SistemasLineares.h"

int main ()
{
  double err, tempo, *A, *B, *Xa, *Xb;
  int n, it, i, j, k;
  char *Divisoria = "#############################################################################";
  size_t nxnSize, nx1Size;

  fprintf(stdout, "Entre com o tamanho do sistema: ");
  fscanf(stdin, "%d", &n);

  nx1Size = (size_t) n * sizeof(double);
  nxnSize = (size_t) n * nx1Size;

  A = (double *) malloc(nxnSize);

  fprintf(stdout, "Entre com os coeficientes do sistema: ");
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      fscanf(stdin, "%lf", &A[i * n + j]);

  B = (double *) malloc(nx1Size);

  fprintf(stdout, "Entre com os termos independentes do sistema: ");
  for (k = 0; k < n; k++)
    fscanf(stdin, "%lf", &B[k]);

  Xa = (double *) malloc(nx1Size);

  fprintf(stdout, "Entre com o chute inicial: ");
  for (k = 0; k < n; k++)
    fscanf(stdin, "%lf", &Xa[k]);

  Xb = (double *) memcpy(malloc(nx1Size), (const void *) Xa, nx1Size);
 
  err = jacobi(A, B, &Xa, n, &it, &tempo);

  fprintf(stdout, "\n%s\n", Divisoria);
  fprintf(stdout, "Metodo de Jacobi");
  fprintf(stdout, "\n%s\n", Divisoria);
  fprintf(stdout, "Erro: %.15g\n", err);
  fprintf(stdout, "Iteracoes: %d\n", it);
  fprintf(stdout, "Tempo/iteracao: %.15g\n", tempo / it);

  fprintf(stdout, "X:");

  for (k = 0; k < n; k++)
    fprintf(stdout, " %.15g", Xa[k]);

  err = gaussSeidel(A, B, &Xb, n, &it, &tempo);

  fprintf(stdout, "\n%s\n", Divisoria);
  fprintf(stdout, "Metodo de Gauss-Seidel");
  fprintf(stdout, "\n%s\n", Divisoria);
  fprintf(stdout, "Erro: %.15g\n", err);
  fprintf(stdout, "Iteracoes: %d\n", it);
  fprintf(stdout, "Tempo/iteracao: %.15g\n", tempo / it);

  fprintf(stdout, "X:");

  for (k = 0; k < n; k++)
    fprintf(stdout, " %.15g", Xb[k]);

  fprintf(stdout, "\n%s\n", Divisoria);

  free(A);
  free(B);
  free(Xa);
  free(Xb);

  return 0;
}
