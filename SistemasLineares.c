#include <stdio.h>
#include <math.h>

#include "utils.h"
#include "SistemasLineares.h"

// Método de Jacobi
double  jacobi (double  *A, double *B,  double **X,  int n,
		int *tIteracao, double *tTotal)
{
  *tTotal = timestamp();

  double soma, err, auxVal, auxVal2, *auxPtr, *X0, *X1;

  int i, j;

  X0 = *X;
  X1 = (double *) malloc(n * sizeof(double));
  err = 0.0;

  for (i = 0; i < n; i++) {
    soma = 0.0;

    for (j = 0; j < i; j++)
      soma += A[i * n + j] * X0[j];

    auxVal = A[i * n + i];
    auxVal2 = X0[i];

    for (j = i + 1; j < n; j++)
      soma += A[i * n + j] * X0[j];

    auxVal = B[i] / auxVal - soma / auxVal;
    X1[i] = auxVal;

    if ((auxVal = fabs(auxVal - auxVal2)) > err )
      err = auxVal;
  }

  for ((*tIteracao) = 1; (*tIteracao) < MAXIT && err >= EPS ; (*tIteracao)++) {
    auxPtr = X0;
    X0 = X1;
    X1 = auxPtr;

    err = 0.0;

    for (i = 0; i < n; i++) {
      soma = 0.0;

      for (j = 0; j < i; j++)
        soma += A[i * n + j] * X0[j];

      auxVal = A[i * n + i];
      auxVal2 = X0[i];

      for (j = i + 1; j < n; j++)
        soma += A[i * n + j] * X0[j];

      auxVal = B[i] / auxVal - soma / auxVal;
      X1[i] = auxVal;

      if ((auxVal = fabs(auxVal - auxVal2)) > err )
        err = auxVal;
    }
  }

  free(X0);
  *X = X1;

  *tTotal = timestamp() - *tTotal;

  return err;
}

// Método de Gauss-Seidel
double  gaussSeidel (double  *A, double *B,  double **X,  int n,
		    int *tIteracao, double *tTotal)
{
  *tTotal = timestamp();

  double soma, err, auxVal, auxVal2, *auxPtr, *X0, *X1;

  int i, j;

  X0 = *X;
  X1 = (double *) malloc(n * sizeof(double));
  err = 0.0;

  for (i = 0; i < n; i++) {
    soma = 0.0;

    for (j = 0; j < i; j++)
      soma += A[i * n + j] * X1[j];

    auxVal = A[i * n + i];
    auxVal2 = X0[i];

    for (j = i + 1; j < n; j++)
      soma += A[i * n + j] * X0[j];

    auxVal = B[i] / auxVal - soma / auxVal;
    X1[i] = auxVal;

    if ((auxVal = fabs(auxVal - auxVal2)) > err )
      err = auxVal;
  }

  for ((*tIteracao) = 1; (*tIteracao) < MAXIT && err >= EPS ; (*tIteracao)++) {
    auxPtr = X0;
    X0 = X1;
    X1 = auxPtr;

    err = 0.0;

    for (i = 0; i < n; i++) {
      soma = 0.0;

      for (j = 0; j < i; j++)
        soma += A[i * n + j] * X1[j];

      auxVal = A[i * n + i];
      auxVal2 = X0[i];

      for (j = i + 1; j < n; j++)
        soma += A[i * n + j] * X0[j];

      auxVal = B[i] / auxVal - soma / auxVal;
      X1[i] = auxVal;

      if ((auxVal = fabs(auxVal - auxVal2)) > err )
        err = auxVal;
    }
  }

  free(X0);
  *X = X1;

  *tTotal = timestamp() - *tTotal;

  return err;
}


